package com.isw.svcord023.domain.svcord.command;


import org.springframework.stereotype.Service;
import com.isw.svcord023.sdk.domain.svcord.command.Svcord023CommandBase;
import com.isw.svcord023.sdk.domain.svcord.entity.Svcord023Entity;
import com.isw.svcord023.sdk.domain.svcord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord023.sdk.domain.svcord.type.ServicingOrderType;
import com.isw.svcord023.sdk.domain.svcord.type.ServicingOrderWorkProduct;
import com.isw.svcord023.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord023.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord023.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord023Command extends Svcord023CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord023Command.class);

  public Svcord023Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }

  
    
    @Override
    public com.isw.svcord023.sdk.domain.svcord.entity.Svcord023 createServicingOrderProducer(com.isw.svcord023.sdk.domain.svcord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
    
      Svcord023Entity servicingOrderProcedure = this.entityBuilder.getSvcord().getSvcord023().build();
      servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessStartDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
      servicingOrderProcedure.setServicingOrderWorkDescription("CASH WITHDRAWALS");
      servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
      servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);
      
      ThirdPartyReferenceEntity thirdPartyReference = new ThirdPartyReferenceEntity();
      thirdPartyReference.setId("test1");
      thirdPartyReference.setPassword("password");
      servicingOrderProcedure.setThirdPartyReference(thirdPartyReference);
   
      return repo.getSvcord().getSvcord023().save(servicingOrderProcedure);
   
    }
  
    
    @Override
    public void updateServicingOrderProducer(com.isw.svcord023.sdk.domain.svcord.entity.Svcord023 instance, com.isw.svcord023.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

      Svcord023Entity servicingOrderProcedure = this.repo.getSvcord().getSvcord023().getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
      servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessEndDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
      servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());
    
      log.info(updateServicingOrderProducerInput.getUpdateID().toString());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());
    
      this.repo.getSvcord().getSvcord023().save(servicingOrderProcedure);
    
      
    }
  
}
